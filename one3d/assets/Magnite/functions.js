var one3dFunctions = function () { this.loadedPercent = 0; this.variantName = ""; serverURL = ""; canvasId = ""; carName = ""; };
var ONE3D = new one3dFunctions();
var Headlight_Toggle = false;
var Taillight_Toggle = false;
var hideflag = false;
var LastColorSelect = "rusticbrown_whiteroof";
var nissanstaticfolder = document.currentScript.getAttribute("data-url");
var untouched = true;
var cameratutorialanim;
var handanim;
var thehand;
var CurrentFeatName = null;
var selectedvariable = "";
var changevar = false
var defaultcolor = "";
var currentfeature = undefined;
var timeouts = [];
var timeoutsLight = [];
var lightInterval = null;
var timeouts = [];
var timeoutsLight = [];
var lightInterval = null;

one3dFunctions.prototype.init = function (component_id = '', serverURL = '/assets/', carName = "Magnite", variant = "Turbo CVT XV Pre",options={}) {

    console.log("12-01-2021");
    if (component_id == '') return 0;
    ONE3D.canvasId = "one3dcanvas";
    ONE3D.carName = carName;
    ONE3D.variant = variant;
    ONE3D.serverURL = serverURL + "assets/"

    var options = options || {};
    var preConfig = options.preConfig || null

    var fileurl = serverURL;
    var UIcomponent = document.getElementById(component_id);
    UIcomponent.innerHTML = `<div id="canvasContainer">
		<canvas id="`+ ONE3D.canvasId + `" touch-action="none"
		  style="width: 100%;height: 100%;position: absolute;outline: 0;"></canvas>
		</div>

		<div id="cameraFade"
		style="background-color:#C3002F;width: 100%;height: 100%;position: absolute; top: 0px; overflow-x: hidden;overflow-y: hidden;left: 0px;">  
		</div>

		<div id="loader_BG"
		style="background-color:#C3002F;width: 100%;height: 100%;position: absolute; top: 0px;overflow-x: hidden;overflow-y: hidden;left: 0px;z-index:99;">
		<img src="`+ `one3d/images/nissan.svg" class="one3d-loader-logo" style="display: block;position: absolute;transform: translate(50%, -50%);top: 50%;right: 50%;z-index:9999;">
       
        <div class="one3d-loader-percentage">
        <div class="loader-new"><div id="myBar"></div></div>
        </div>
        </div>
        </div>

        <div id="hand" style="position: absolute;
        top: 50%;
        left: 50%;
        opacity:0;
        transform: translate(-50%, -50%);"><img src="`+ `one3d/images/hand.png" style="height: 45px;"></div>`;
    var element = document.getElementById('canvasContainer');
    if (typeof (element) != 'undefined' && element != null) {
        var script = document.createElement("script");
        script.src = fileurl + "package.js";
        script.type = "text/javascript";
        script.onload = function () {
            ONE3D.LoadCar(carName, variant, showfeature = true, preConfig);
        };
        document.getElementsByTagName("head")[0].appendChild(script);
    }
    //Sounds
    if (typeof (element) != 'undefined' && element != null) {
        var script = document.createElement("script");
        script.src = fileurl + "howler.min.js";
        script.type = "text/javascript";
        script.onload = function () {
            preloadsounds();
        };
        document.getElementsByTagName("head")[0].appendChild(script);
    }
};

one3dFunctions.prototype.InteriorView = function () {
    window.one3dref._gotoInterior();
}
one3dFunctions.prototype.ExteriorView = function () {
    window.one3dref._gotoExterior();
}
one3dFunctions.prototype.FrontSeatView = function () {
    window.one3dref._gotoFrontSeat();
}
one3dFunctions.prototype.RearSeatView = function () {
    window.one3dref._gotoBackSeat();
}

one3dFunctions.prototype.changeBgColor = function (value) {
    window.one3dref._changeBGcolor(value);
}

one3dFunctions.prototype.ResetCamera = function () {
    window.one3dref._resetCamera();
}

one3dFunctions.prototype.DefaultPreloader = function (value) {
    window.one3dref._DefaultPreloader(value);
}
one3dFunctions.prototype.changeVariant = function (value, getFeature = false) {
    // ONE3D.hideFeature();
    if (ONE3D.variantName === value) {
        return;
    }

    //Remove Camera Tutorial
    if (untouched == true) {
        removecameratutorial();
    }
    if (lightOn) ONE3D.ToggleLights();
    if (doorOpen) ONE3D.ToggleDoors();
    window.one3dref._changeVariant(value, getFeature);
}

one3dFunctions.prototype.LoadCar = function (carName = 'Magnite', variant = 'Turbo CVT XV Pre', feature = true, preConfig = null) {
    ONE3D.variantName = variant;
    ONE3D.carName = carName;
    window.one3drefMain._setServerURL(ONE3D.serverURL);
    window.one3drefMain._loadCar(carName, variant, { showfeature: true,pin:preConfig });
}


window.addEventListener("resize", function () { ONE3D.resizeWindow(); });

one3dFunctions.prototype.resizeWindow = function () {
    if (window.one3dref)
        window.one3dref._resizeWindow();
}


// Remove Listener
one3dFunctions.prototype.removeEventListener = function () {
    if (_check_orientation)
        window.removeEventListener("orientationchange", _check_orientation, false);

    if (_checkResize)
        window.removeEventListener("resize", _checkResize, false);
}

//Features
one3dFunctions.prototype.showFeature = function (featureName) {
    if (untouched) {
        removecameratutorial();
    }
    if (currentfeature) {
        ONE3D.hideFeature(featureName);
        return;
    }

    currentfeature = featureName;
    if (currentfeature === "FEAT_Wide_Split_Tail_Lamp") {
        if (doorOpen)
            ONE3D.ToggleDoors();
    }

    if (currentfeature === "FEAT_Wide_Split_Tail_Lamp" || currentfeature === "FEAT_Headlamp_LED_Projector") {
        if (lightOn)
            ONE3D.ToggleLights();
    }


    if (currentfeature === "FEAT_AVM_360_Degree") {
        setTimeout(() => {
            one3dref.getMesh('NISSAN_FEAT_AVM_360_ONE_MESH').setEnabled(true);
            one3dref.getMesh('NISSAN_FEAT_AVM_360_TWO_MESH').setEnabled(false);
            one3dref.getMesh('NISSAN_FEAT_AVM_360_THREE_MESH').setEnabled(false);
        }, 3500);
        setTimeout(() => {
            one3dref.getMesh('NISSAN_FEAT_AVM_360_ONE_MESH').setEnabled(false);
            one3dref.getMesh('NISSAN_FEAT_AVM_360_TWO_MESH').setEnabled(true);
            one3dref.getMesh('NISSAN_FEAT_AVM_360_THREE_MESH').setEnabled(false);
        }, 4500);
        setTimeout(() => {
            one3dref.getMesh('NISSAN_FEAT_AVM_360_ONE_MESH').setEnabled(false);
            one3dref.getMesh('NISSAN_FEAT_AVM_360_TWO_MESH').setEnabled(false);
            one3dref.getMesh('NISSAN_FEAT_AVM_360_THREE_MESH').setEnabled(true);
        }, 5500);
    }

    if (currentfeature == 'FEAT_Rear_Armrest_with_Mob_Holder') {
        one3dref.getMesh('MAGNITE_TURBO_CVT_XV_PRE_IN_SEATS_ARMREST_MESH').rotationQuaternion = null;

        var armrest = one3dref.getMesh('MAGNITE_TURBO_CVT_XV_PRE_IN_SEATS_ARMREST_MESH').rotation;
        armrest.z = -1.6;
        jittercam();
        TweenMax.to(armrest, 3, {
            z: 0.19, ease: Power1.easeIn, delay: 2.5, onUpdate: jittercam, onComplete: function () {
                one3dref.getMesh('NISSAN_FEAT_ARMREST_PHONE_MESH').setEnabled(true);
                one3dref.getMesh('NISSAN_FEAT_ARMREST_PHONE_MESH').position.x = 0.01;
            }
        });
    }


    if (window.one3dref)
        window.one3dref._showFeature(featureName);
}
one3dFunctions.prototype.nextFeature = function () {
    if (window.one3dref)
        window.one3dref._nextFeature();
}
one3dFunctions.prototype.previousFeature = function () {
    if (window.one3dref)
        window.one3dref._previousFeature();
}

// var dataV={};
// dataV.videoname="Intrument Cluster.mp4";
// dataV.url="one3d/assets/sounds/";
// dataV.materialname="NISSAN_FEAT_INSTRUMENT_CLUSETER_SCREEN_VIDEO_MAT";
// dataV.loopCount=1;
// dataV.mute=true;
// dataV.flip=true;
// dataV.callBack=false;

one3dFunctions.prototype.playVideo = function (data) {

    if (window.one3dref)
        window.one3dref._featureVideoPlay(data);
}
one3dFunctions.prototype.hideFeature = function (CallFeature) {
    for (var i = 0; i < timeouts.length; i++) {
        clearTimeout(timeouts[i]);
    }
    timeouts = [];
    if (currentfeature === "FEAT_AVM_360_Degree") {
        one3dref.getMesh('NISSAN_FEAT_AVM_360_ONE_MESH').setEnabled(false);
        one3dref.getMesh('NISSAN_FEAT_AVM_360_TWO_MESH').setEnabled(false);
        one3dref.getMesh('NISSAN_FEAT_AVM_360_THREE_MESH').setEnabled(false);
    }


    if (currentfeature == 'FEAT_Rear_Armrest_with_Mob_Holder') {
        one3dref.getMesh('MAGNITE_TURBO_CVT_XV_PRE_IN_SEATS_ARMREST_MESH').rotationQuaternion = null;

        var armrest = one3dref.getMesh('MAGNITE_TURBO_CVT_XV_PRE_IN_SEATS_ARMREST_MESH').rotation;
        armrest.z = 0;
        one3dref.getMesh('NISSAN_FEAT_ARMREST_PHONE_MESH').setEnabled(false)
    }

    TweenMax.killAll();
    currentfeature = undefined;
    if (CallFeature) {
        ONE3D.showFeature(CallFeature);
    }
    else {
        if (window.one3dref)
            window.one3dref._hideFeature();
    }
}
one3dFunctions.prototype.setOffset = function (name, val1, val2) {
    if (window.one3dref)
        window.one3dref._setOffset(name, val1, val2);
}


//Lights
one3dFunctions.prototype.enableHeadlight = function (enable) {
    if (typeof enable === "boolean") {
        Headlight_Toggle = enable;
    }

    if (window.one3dref)
        window.one3dref._enableHeadLight(enable);
}

one3dFunctions.prototype.enableTaillight = function (enable) {
    if (typeof enable === "boolean") {
        Taillight_Toggle = enable;
    }

    if (window.one3dref)
        window.one3dref._enableTailLight(enable);
}

one3dFunctions.prototype.HeadlightToggle = function () {
    Headlight_Toggle = !Headlight_Toggle;
    if (window.one3dref)
        window.one3dref._enableHeadLight(Headlight_Toggle);
}

one3dFunctions.prototype.TaillightToggle = function () {
    Taillight_Toggle = !Taillight_Toggle;
    if (window.one3dref)
        window.one3dref._enableTailLight(Taillight_Toggle);
}


//  document.addEventListener('DOMContentLoaded', function () {
      
//  }, false);
//Camera Tutorial
function cameratutorial() {
    if (untouched == true) {
        thehand = document.getElementById("hand");
        TweenMax.to(thehand, 0.5, { opacity: "0.8" });
        handanim = TweenMax.to(thehand, 1, { left: "60%", ease: Power2.easeOut, yoyoEase: Power2.easeOut, repeat: 3 });
        cameratutorialanim = TweenMax.to(one3dref.getScene().cameras[0], 1, { alpha: one3dref.getScene().cameras[0].alpha - 0.1, ease: Power2.easeOut, yoyoEase: Power2.easeOut, repeat: 3, onUpdate: updatecam, onComplete: checkforcamtutorial });
    }
    document.getElementById(ONE3D.canvasId).addEventListener('pointerdown', removecameratutorial, false);
}

function updatecam() {
    ONE3D.resizeWindow();
}

function checkforcamtutorial() {
    TweenMax.to(thehand, 0.5, { opacity: "0" });
    setTimeout(function () {
        if (untouched == true) {
            thehand = document.getElementById("hand");
            TweenMax.to(thehand, 0.5, { opacity: "0.8", display: "block" });
            handanim = TweenMax.to(thehand, 1, { left: "60%", ease: Power2.easeOut, yoyoEase: Power2.easeOut, repeat: 3 });
            cameratutorialanim = TweenMax.to(one3dref.getScene().cameras[0], 1, { alpha: one3dref.getScene().cameras[0].alpha - 0.1, ease: Power2.easeOut, yoyoEase: Power2.easeOut, repeat: 3, onUpdate: updatecam, onComplete: checkforcamtutorial });
        }
    }, 5000);

}

function removecameratutorial() {
    untouched = false;
    if (cameratutorialanim) {
        cameratutorialanim.kill();
    }
    if (handanim) {
        handanim.kill();
        handanim = null;
    }
    TweenMax.to(thehand, 0.5, { opacity: "0", display: "none" });
    document.getElementById(ONE3D.canvasId).removeEventListener('pointerdown', function () { });
}

//Door Open
function jittercam() {
    one3dref.getScene().cameras[0].alpha += 0.00001;
    ONE3D.resizeWindow();
}

var leftfrontdoor, rightfrontdoor, leftreardoor, rightreardoor, bootdoor, bootdoormech;
var doorOpen = false;

//Alpha rendering 
function checkalphaindex() {
    if (one3dref.getMesh('TOYOTA_URBAN_CRUISER_IN_DECAL_MESH_primitive1')) {
        one3dref.getMesh('TOYOTA_URBAN_CRUISER_IN_DECAL_MESH_primitive1').alphaIndex = 1;
    }

    if (one3dref.getMesh('TOYOTA_URBAN_CRUISER_IN_DECAL_MESH_primitive4')) {
        one3dref.getMesh('TOYOTA_URBAN_CRUISER_IN_DECAL_MESH_primitive4').alphaIndex = 1;
    }

}
//Environment Switch
one3dFunctions.prototype.ChangeEnv = function (value) {
    var scene = one3dref.getScene();
    var env1 = one3dref.getHDR("exterior");
    var env2 = one3dref.getHDR("env1");
    var _setting = one3dref.getSettingJson();
    var Dome = one3dref.getMesh("GLOSTER_DOME_MESH");

    switch (value) {
        case "day":
            if (Dome) Dome.setEnabled(true);
            one3dref._ChangeEnv("day");
            if (one3dref._InInterior() != true) {
                scene.environmentTexture = env1;
                scene.imageProcessingConfiguration.exposure =
                    _setting.scene.skyBox.dayExterior.exposure;
                scene.imageProcessingConfiguration.contrast =
                    _setting.scene.skyBox.dayExterior.contrast;
                one3dref.getMesh("MAGNITE_ENV_NIGHT_DOME").setEnabled(false);
                one3dref.getMesh("MAGNITE_ENV_NIGHT_SHADOW").setEnabled(false);
                // one3dref.getMesh('MAGNITE_ENV_NIGHT_LIGHTS').setEnabled(false);
                one3dref.getMesh("MAGNITE_ENV_SHADOW").setEnabled(true);
                one3dref.getMesh("MAGNITE_ENV_DOME").setEnabled(true);
                lightOn = true;
                ONE3D.ToggleLights();
            }
            one3dref._resizeWindow();
            break;

        case "night":
            if (Dome) Dome.setEnabled(false);
            one3dref._ChangeEnv("env1");
            if (one3dref._InInterior() != true) {
                scene.environmentTexture = env2;
                scene.imageProcessingConfiguration.exposure =
                    _setting.scene.skyBox.ENV_1.exposure;
                scene.imageProcessingConfiguration.contrast =
                    _setting.scene.skyBox.ENV_1.contrast;
                one3dref.getMesh("MAGNITE_ENV_NIGHT_DOME").setEnabled(true);
                one3dref.getMesh("MAGNITE_ENV_NIGHT_SHADOW").setEnabled(true);
                // one3dref.getMesh('MAGNITE_ENV_NIGHT_LIGHTS').setEnabled(true);
                one3dref.getMesh("MAGNITE_ENV_SHADOW").setEnabled(false);
                one3dref.getMesh("MAGNITE_ENV_DOME").setEnabled(false);
                lightOn = false;
                ONE3D.ToggleLights();
            }
            one3dref._resizeWindow();
            break;

        default:
            break;
    }
};

//Sounds
var dooropensound, doorclosesound;
function preloadsounds() {
    dooropensound = new Howl({
        src: ['one3d/assets/sounds/CarDoorOpen.mp3']
    });

    doorclosesound = new Howl({
        src: ['one3d/assets/sounds/CarDoorClose.mp3']
    });
}

//Door Open
function jittercam() {
    one3dref.getScene().cameras[0].alpha += 0.00001;
    ONE3D.resizeWindow();
}

var leftfrontdoor, rightfrontdoor, leftreardoor, rightreardoor, bootdoor, bootdoormech, bootdoormechglow, orvmLight1, orvmLight2;
var doorOpen = false;
one3dFunctions.prototype.ToggleDoors = function ToggleDoors() {
    if (ONE3D.variantName == 'Turbo CVT XV Pre') {
        leftfrontdoor = one3dref.getMesh('MAGNITE_TURBO_CVT_XV_PRE_EX_L_DOOR_MESH');
        rightfrontdoor = one3dref.getMesh('MAGNITE_TURBO_CVT_XV_PRE_EX_R_DOOR_MESH');
        leftreardoor = one3dref.getMesh('MAGNITE_TURBO_CVT_XV_PRE_EX_LR_DOOR_MESH');
        rightreardoor = one3dref.getMesh('MAGNITE_TURBO_CVT_XV_PRE_EX_RR_DOOR_MESH');
        bootdoor = one3dref.getMesh('MAGNITE_TURBO_CVT_XV_PRE_EX_BOOTDOOR_MESH');
        bootdoormech = one3dref.getMesh('MAGNITE_TURBO_CVT_XV_PRE_EX_BOOTDOORMECH_MESH');
        bootdoormechglow = one3dref.getMesh('NISSAN_FEAT_TAIL_GLOW_BOOTDOOR_MESH');
        orvmLight1 = one3dref.getMesh("MAGNITE_TURBO_CVT_XV_PRE_EX_L_DOOR_INDICATOR_MESH");
        orvmLight2 = one3dref.getMesh("MAGNITE_TURBO_CVT_XV_PRE_EX_R_DOOR_INDICATOR_MESH");

        if (orvmLight1 != null)
            one3dref.getMesh("MAGNITE_TURBO_CVT_XV_PRE_EX_L_DOOR_INDICATOR_MESH").rotationQuaternion = null;
        if (orvmLight2 != null)
            one3dref.getMesh("MAGNITE_TURBO_CVT_XV_PRE_EX_R_DOOR_INDICATOR_MESH").rotationQuaternion = null
        one3dref.getMesh('MAGNITE_TURBO_CVT_XV_PRE_EX_L_DOOR_MESH').rotationQuaternion = null;
        one3dref.getMesh('MAGNITE_TURBO_CVT_XV_PRE_EX_R_DOOR_MESH').rotationQuaternion = null;
        one3dref.getMesh('MAGNITE_TURBO_CVT_XV_PRE_EX_LR_DOOR_MESH').rotationQuaternion = null;
        one3dref.getMesh('MAGNITE_TURBO_CVT_XV_PRE_EX_RR_DOOR_MESH').rotationQuaternion = null;
        one3dref.getMesh('MAGNITE_TURBO_CVT_XV_PRE_EX_BOOTDOOR_MESH').rotationQuaternion = null;
        one3dref.getMesh('MAGNITE_TURBO_CVT_XV_PRE_EX_BOOTDOORMECH_MESH').rotationQuaternion = null;
        if (bootdoormechglow != null)
            one3dref.getMesh('NISSAN_FEAT_TAIL_GLOW_BOOTDOOR_MESH').rotationQuaternion = null;

    }
    if (ONE3D.variantName == 'Turbo MT XV Pre') {
        leftfrontdoor = one3dref.getMesh('MAGNITE_TURBO_CVT_XV_PRE_EX_L_DOOR_MESH');
        rightfrontdoor = one3dref.getMesh('MAGNITE_TURBO_CVT_XV_PRE_EX_R_DOOR_MESH');
        leftreardoor = one3dref.getMesh('MAGNITE_TURBO_CVT_XV_PRE_EX_LR_DOOR_MESH');
        rightreardoor = one3dref.getMesh('MAGNITE_TURBO_CVT_XV_PRE_EX_RR_DOOR_MESH');
        bootdoor = one3dref.getMesh('MAGNITE_TURBO_MT_XV_PRE_EX_BOOTDOOR_MESH');
        bootdoormech = one3dref.getMesh('MAGNITE_TURBO_CVT_XV_PRE_EX_BOOTDOORMECH_MESH');
        bootdoormechglow = one3dref.getMesh('NISSAN_FEAT_TAIL_GLOW_BOOTDOOR_MESH');

        one3dref.getMesh('MAGNITE_TURBO_CVT_XV_PRE_EX_L_DOOR_MESH').rotationQuaternion = null;
        one3dref.getMesh('MAGNITE_TURBO_CVT_XV_PRE_EX_R_DOOR_MESH').rotationQuaternion = null;
        one3dref.getMesh('MAGNITE_TURBO_CVT_XV_PRE_EX_LR_DOOR_MESH').rotationQuaternion = null;
        one3dref.getMesh('MAGNITE_TURBO_CVT_XV_PRE_EX_RR_DOOR_MESH').rotationQuaternion = null;
        one3dref.getMesh('MAGNITE_TURBO_MT_XV_PRE_EX_BOOTDOOR_MESH').rotationQuaternion = null;
        one3dref.getMesh('MAGNITE_TURBO_CVT_XV_PRE_EX_BOOTDOORMECH_MESH').rotationQuaternion = null;
        if (bootdoormechglow != null)
            one3dref.getMesh('NISSAN_FEAT_TAIL_GLOW_BOOTDOOR_MESH').rotationQuaternion = null;
    }
    if (ONE3D.variantName == 'Turbo MT XV') {
        leftfrontdoor = one3dref.getMesh('MAGNITE_TURBO_CVT_XV_UPR_EX_L_DOOR_MESH');
        rightfrontdoor = one3dref.getMesh('MAGNITE_TURBO_CVT_XV_UPR_EX_R_DOOR_MESH');
        leftreardoor = one3dref.getMesh('MAGNITE_TURBO_CVT_XV_UPR_EX_LR_DOOR_MESH');
        rightreardoor = one3dref.getMesh('MAGNITE_TURBO_CVT_XV_UPR_EX_RR_DOOR_MESH');
        bootdoor = one3dref.getMesh('MAGNITE_TURBO_MT_XV_PRE_EX_BOOTDOOR_MESH');
        bootdoormech = one3dref.getMesh('MAGNITE_TURBO_CVT_XV_PRE_EX_BOOTDOORMECH_MESH');
        bootdoormechglow = one3dref.getMesh('NISSAN_FEAT_TAIL_GLOW_BOOTDOOR_MESH');

        one3dref.getMesh('MAGNITE_TURBO_CVT_XV_UPR_EX_L_DOOR_MESH').rotationQuaternion = null;
        one3dref.getMesh('MAGNITE_TURBO_CVT_XV_UPR_EX_R_DOOR_MESH').rotationQuaternion = null;
        one3dref.getMesh('MAGNITE_TURBO_CVT_XV_UPR_EX_LR_DOOR_MESH').rotationQuaternion = null;
        one3dref.getMesh('MAGNITE_TURBO_CVT_XV_UPR_EX_RR_DOOR_MESH').rotationQuaternion = null;
        one3dref.getMesh('MAGNITE_TURBO_MT_XV_PRE_EX_BOOTDOOR_MESH').rotationQuaternion = null;
        one3dref.getMesh('MAGNITE_TURBO_CVT_XV_PRE_EX_BOOTDOORMECH_MESH').rotationQuaternion = null;
        if (bootdoormechglow != null)
            one3dref.getMesh('NISSAN_FEAT_TAIL_GLOW_BOOTDOOR_MESH').rotationQuaternion = null;
    }
    if (ONE3D.variantName == 'Turbo MT XL') {
        leftfrontdoor = one3dref.getMesh('MAGNITE_TURBO_CVT_XL_EX_L_DOOR_MESH');
        rightfrontdoor = one3dref.getMesh('MAGNITE_TURBO_CVT_XL_EX_R_DOOR_MESH');
        leftreardoor = one3dref.getMesh('MAGNITE_TURBO_CVT_XL_EX_LR_DOOR_MESH');
        rightreardoor = one3dref.getMesh('MAGNITE_TURBO_CVT_XL_EX_RR_DOOR_MESH');
        bootdoor = one3dref.getMesh('MAGNITE_TURBO_XL_EX_BOOTDOOR_MESH');
        bootdoormech = one3dref.getMesh('MAGNITE_TURBO_CVT_XV_PRE_EX_BOOTDOORMECH_MESH');
        bootdoormechglow = one3dref.getMesh('NISSAN_FEAT_TAIL_GLOW_BOOTDOOR_MESH');

        one3dref.getMesh('MAGNITE_TURBO_CVT_XL_EX_L_DOOR_MESH').rotationQuaternion = null;
        one3dref.getMesh('MAGNITE_TURBO_CVT_XL_EX_R_DOOR_MESH').rotationQuaternion = null;
        one3dref.getMesh('MAGNITE_TURBO_CVT_XL_EX_LR_DOOR_MESH').rotationQuaternion = null;
        one3dref.getMesh('MAGNITE_TURBO_CVT_XL_EX_RR_DOOR_MESH').rotationQuaternion = null;
        one3dref.getMesh('MAGNITE_TURBO_XL_EX_BOOTDOOR_MESH').rotationQuaternion = null;
        one3dref.getMesh('MAGNITE_TURBO_CVT_XV_PRE_EX_BOOTDOORMECH_MESH').rotationQuaternion = null;
        if (bootdoormechglow != null)
            one3dref.getMesh('NISSAN_FEAT_TAIL_GLOW_BOOTDOOR_MESH').rotationQuaternion = null;
    }
    if (ONE3D.variantName == 'Turbo CVT XV') {
        leftfrontdoor = one3dref.getMesh('MAGNITE_TURBO_CVT_XV_UPR_EX_L_DOOR_MESH');
        rightfrontdoor = one3dref.getMesh('MAGNITE_TURBO_CVT_XV_UPR_EX_R_DOOR_MESH');
        leftreardoor = one3dref.getMesh('MAGNITE_TURBO_CVT_XV_UPR_EX_LR_DOOR_MESH');
        rightreardoor = one3dref.getMesh('MAGNITE_TURBO_CVT_XV_UPR_EX_RR_DOOR_MESH');
        bootdoor = one3dref.getMesh('MAGNITE_TURBO_CVT_XV_PRE_EX_BOOTDOOR_MESH');
        bootdoormech = one3dref.getMesh('MAGNITE_TURBO_CVT_XV_PRE_EX_BOOTDOORMECH_MESH');
        bootdoormechglow = one3dref.getMesh('NISSAN_FEAT_TAIL_GLOW_BOOTDOOR_MESH');

        one3dref.getMesh('MAGNITE_TURBO_CVT_XV_UPR_EX_L_DOOR_MESH').rotationQuaternion = null;
        one3dref.getMesh('MAGNITE_TURBO_CVT_XV_UPR_EX_R_DOOR_MESH').rotationQuaternion = null;
        one3dref.getMesh('MAGNITE_TURBO_CVT_XV_UPR_EX_LR_DOOR_MESH').rotationQuaternion = null;
        one3dref.getMesh('MAGNITE_TURBO_CVT_XV_UPR_EX_RR_DOOR_MESH').rotationQuaternion = null;
        one3dref.getMesh('MAGNITE_TURBO_CVT_XV_PRE_EX_BOOTDOOR_MESH').rotationQuaternion = null;
        one3dref.getMesh('MAGNITE_TURBO_CVT_XV_PRE_EX_BOOTDOORMECH_MESH').rotationQuaternion = null;
        if (bootdoormechglow != null)
            one3dref.getMesh('NISSAN_FEAT_TAIL_GLOW_BOOTDOOR_MESH').rotationQuaternion = null;
    }
    if (ONE3D.variantName == 'Turbo CVT XL') {
        leftfrontdoor = one3dref.getMesh('MAGNITE_TURBO_CVT_XL_EX_L_DOOR_MESH');
        rightfrontdoor = one3dref.getMesh('MAGNITE_TURBO_CVT_XL_EX_R_DOOR_MESH');
        leftreardoor = one3dref.getMesh('MAGNITE_TURBO_CVT_XL_EX_LR_DOOR_MESH');
        rightreardoor = one3dref.getMesh('MAGNITE_TURBO_CVT_XL_EX_RR_DOOR_MESH');
        bootdoor = one3dref.getMesh('MAGNITE_TURBO_CVT_XL_EX_BOOTDOOR_MESH');
        bootdoormech = one3dref.getMesh('MAGNITE_TURBO_CVT_XV_PRE_EX_BOOTDOORMECH_MESH');
        bootdoormechglow = one3dref.getMesh('NISSAN_FEAT_TAIL_GLOW_BOOTDOOR_MESH');

        one3dref.getMesh('MAGNITE_TURBO_CVT_XL_EX_L_DOOR_MESH').rotationQuaternion = null;
        one3dref.getMesh('MAGNITE_TURBO_CVT_XL_EX_R_DOOR_MESH').rotationQuaternion = null;
        one3dref.getMesh('MAGNITE_TURBO_CVT_XL_EX_LR_DOOR_MESH').rotationQuaternion = null;
        one3dref.getMesh('MAGNITE_TURBO_CVT_XL_EX_RR_DOOR_MESH').rotationQuaternion = null;
        one3dref.getMesh('MAGNITE_TURBO_CVT_XL_EX_BOOTDOOR_MESH').rotationQuaternion = null;
        one3dref.getMesh('MAGNITE_TURBO_CVT_XV_PRE_EX_BOOTDOORMECH_MESH').rotationQuaternion = null;
        if (bootdoormechglow != null)
            one3dref.getMesh('NISSAN_FEAT_TAIL_GLOW_BOOTDOOR_MESH').rotationQuaternion = null;
    }
    if (ONE3D.variantName == 'MT XV Pre') {
        leftfrontdoor = one3dref.getMesh('MAGNITE_TURBO_CVT_XV_PRE_EX_L_DOOR_MESH');
        rightfrontdoor = one3dref.getMesh('MAGNITE_TURBO_CVT_XV_PRE_EX_R_DOOR_MESH');
        leftreardoor = one3dref.getMesh('MAGNITE_TURBO_CVT_XV_PRE_EX_LR_DOOR_MESH');
        rightreardoor = one3dref.getMesh('MAGNITE_TURBO_CVT_XV_PRE_EX_RR_DOOR_MESH');
        bootdoor = one3dref.getMesh('MAGNITE_MT_XV_PRE_EX_BOOTDOOR_MESH');
        bootdoormech = one3dref.getMesh('MAGNITE_TURBO_CVT_XV_PRE_EX_BOOTDOORMECH_MESH');
        bootdoormechglow = one3dref.getMesh('NISSAN_FEAT_TAIL_GLOW_BOOTDOOR_MESH');

        one3dref.getMesh('MAGNITE_TURBO_CVT_XV_PRE_EX_L_DOOR_MESH').rotationQuaternion = null;
        one3dref.getMesh('MAGNITE_TURBO_CVT_XV_PRE_EX_R_DOOR_MESH').rotationQuaternion = null;
        one3dref.getMesh('MAGNITE_TURBO_CVT_XV_PRE_EX_LR_DOOR_MESH').rotationQuaternion = null;
        one3dref.getMesh('MAGNITE_TURBO_CVT_XV_PRE_EX_RR_DOOR_MESH').rotationQuaternion = null;
        one3dref.getMesh('MAGNITE_MT_XV_PRE_EX_BOOTDOOR_MESH').rotationQuaternion = null;
        one3dref.getMesh('MAGNITE_TURBO_CVT_XV_PRE_EX_BOOTDOORMECH_MESH').rotationQuaternion = null;
        if (bootdoormechglow != null)
            one3dref.getMesh('NISSAN_FEAT_TAIL_GLOW_BOOTDOOR_MESH').rotationQuaternion = null;
    }
    if (ONE3D.variantName == 'MT XV') {
        leftfrontdoor = one3dref.getMesh('MAGNITE_TURBO_CVT_XV_UPR_EX_L_DOOR_MESH');
        rightfrontdoor = one3dref.getMesh('MAGNITE_TURBO_CVT_XV_UPR_EX_R_DOOR_MESH');
        leftreardoor = one3dref.getMesh('MAGNITE_TURBO_CVT_XV_UPR_EX_LR_DOOR_MESH');
        rightreardoor = one3dref.getMesh('MAGNITE_TURBO_CVT_XV_UPR_EX_RR_DOOR_MESH');
        bootdoor = one3dref.getMesh('MAGNITE_MT_XV_PRE_EX_BOOTDOOR_MESH');
        bootdoormech = one3dref.getMesh('MAGNITE_TURBO_CVT_XV_PRE_EX_BOOTDOORMECH_MESH');
        bootdoormechglow = one3dref.getMesh('NISSAN_FEAT_TAIL_GLOW_BOOTDOOR_MESH');

        one3dref.getMesh('MAGNITE_TURBO_CVT_XV_UPR_EX_L_DOOR_MESH').rotationQuaternion = null;
        one3dref.getMesh('MAGNITE_TURBO_CVT_XV_UPR_EX_R_DOOR_MESH').rotationQuaternion = null;
        one3dref.getMesh('MAGNITE_TURBO_CVT_XV_UPR_EX_LR_DOOR_MESH').rotationQuaternion = null;
        one3dref.getMesh('MAGNITE_TURBO_CVT_XV_UPR_EX_RR_DOOR_MESH').rotationQuaternion = null;
        one3dref.getMesh('MAGNITE_MT_XV_PRE_EX_BOOTDOOR_MESH').rotationQuaternion = null;
        one3dref.getMesh('MAGNITE_TURBO_CVT_XV_PRE_EX_BOOTDOORMECH_MESH').rotationQuaternion = null;
        if (bootdoormechglow != null)
            one3dref.getMesh('NISSAN_FEAT_TAIL_GLOW_BOOTDOOR_MESH').rotationQuaternion = null;
    }
    if (ONE3D.variantName == 'MT XL') {
        leftfrontdoor = one3dref.getMesh('MAGNITE_TURBO_CVT_XL_EX_L_DOOR_MESH');
        rightfrontdoor = one3dref.getMesh('MAGNITE_TURBO_CVT_XL_EX_R_DOOR_MESH');
        leftreardoor = one3dref.getMesh('MAGNITE_TURBO_CVT_XL_EX_LR_DOOR_MESH');
        rightreardoor = one3dref.getMesh('MAGNITE_TURBO_CVT_XL_EX_RR_DOOR_MESH');
        bootdoor = one3dref.getMesh('MAGNITE_MT_XL_XE_EX_BOOTDOOR_MESH');
        bootdoormech = one3dref.getMesh('MAGNITE_TURBO_CVT_XV_PRE_EX_BOOTDOORMECH_MESH');
        bootdoormechglow = one3dref.getMesh('NISSAN_FEAT_TAIL_GLOW_BOOTDOOR_MESH');

        one3dref.getMesh('MAGNITE_TURBO_CVT_XL_EX_L_DOOR_MESH').rotationQuaternion = null;
        one3dref.getMesh('MAGNITE_TURBO_CVT_XL_EX_R_DOOR_MESH').rotationQuaternion = null;
        one3dref.getMesh('MAGNITE_TURBO_CVT_XL_EX_LR_DOOR_MESH').rotationQuaternion = null;
        one3dref.getMesh('MAGNITE_TURBO_CVT_XL_EX_RR_DOOR_MESH').rotationQuaternion = null;
        one3dref.getMesh('MAGNITE_MT_XL_XE_EX_BOOTDOOR_MESH').rotationQuaternion = null;
        one3dref.getMesh('MAGNITE_TURBO_CVT_XV_PRE_EX_BOOTDOORMECH_MESH').rotationQuaternion = null;
        if (bootdoormechglow != null)
            one3dref.getMesh('NISSAN_FEAT_TAIL_GLOW_BOOTDOOR_MESH').rotationQuaternion = null;
    }
    if (ONE3D.variantName == 'MT XE') {
        leftfrontdoor = one3dref.getMesh('MAGNITE_XE_EX_L_DOOR_MESH');
        rightfrontdoor = one3dref.getMesh('MAGNITE_XE_EX_R_DOOR_MESH');
        leftreardoor = one3dref.getMesh('MAGNITE_XE_EX_LR_DOOR_MESH');
        rightreardoor = one3dref.getMesh('MAGNITE_XE_EX_RR_DOOR_MESH');
        bootdoor = one3dref.getMesh('MAGNITE_MT_XL_XE_EX_BOOTDOOR_MESH');
        bootdoormech = one3dref.getMesh('MAGNITE_TURBO_CVT_XV_PRE_EX_BOOTDOORMECH_MESH');
        bootdoormechglow = one3dref.getMesh('NISSAN_FEAT_TAIL_GLOW_BOOTDOOR_MESH');

        one3dref.getMesh('MAGNITE_XE_EX_L_DOOR_MESH').rotationQuaternion = null;
        one3dref.getMesh('MAGNITE_XE_EX_R_DOOR_MESH').rotationQuaternion = null;
        one3dref.getMesh('MAGNITE_XE_EX_LR_DOOR_MESH').rotationQuaternion = null;
        one3dref.getMesh('MAGNITE_XE_EX_RR_DOOR_MESH').rotationQuaternion = null;
        one3dref.getMesh('MAGNITE_MT_XL_XE_EX_BOOTDOOR_MESH').rotationQuaternion = null;
        one3dref.getMesh('MAGNITE_TURBO_CVT_XV_PRE_EX_BOOTDOORMECH_MESH').rotationQuaternion = null;
        if (bootdoormechglow != null)
            one3dref.getMesh('NISSAN_FEAT_TAIL_GLOW_BOOTDOOR_MESH').rotationQuaternion = null;
    }


    if (!doorOpen) {
        dooropensound.play();
        TweenMax.to(leftfrontdoor.rotation, 2, {
            y: -1.05, ease: Power1.easeInOut, onUpdate: jittercam, onComplete: function () {
                doorOpen = true;
            }
        });
        TweenMax.to(leftreardoor.rotation, 2, { y: -1.05, ease: Power1.easeInOut });
        TweenMax.to(rightfrontdoor.rotation, 2, { y: 1.05, ease: Power1.easeInOut });
        TweenMax.to(rightreardoor.rotation, 2, { y: 1.05, ease: Power1.easeInOut });

        TweenMax.to(bootdoor.rotation, 2, { z: 0.91, ease: Power1.easeInOut });
        TweenMax.to(bootdoormech.rotation, 1.9, { z: 1.0, ease: Power1.easeInOut });
        if (bootdoormechglow)
            TweenMax.to(bootdoormechglow.rotation, 2, { z: 0.9, ease: Power1.easeInOut });
        if (orvmLight1 != null) {// console.log(">>>>>>>>")
            TweenMax.to(orvmLight1.rotation, 2, { y: -1.05, ease: Power1.easeInOut });
        }
        if (orvmLight2 != null) {
            TweenMax.to(orvmLight2.rotation, 2, { y: 1.05, ease: Power1.easeInOut });
        }
    }
    else {
        TweenMax.to(leftfrontdoor.rotation, 2, {
            y: 0, ease: Power1.easeIn, onUpdate: jittercam, onComplete: function () {
                doorOpen = false;
                doorclosesound.play();
            }
        });
        TweenMax.to(leftreardoor.rotation, 2, { y: 0, ease: Power1.easeIn });
        TweenMax.to(rightfrontdoor.rotation, 2, { y: 0, ease: Power1.easeIn });
        TweenMax.to(rightreardoor.rotation, 2, { y: 0, ease: Power1.easeIn });

        TweenMax.to(bootdoor.rotation, 2, { z: 0, ease: Power1.easeIn });
        TweenMax.to(bootdoormech.rotation, 2, { z: 0, ease: Power1.easeIn });
        if (bootdoormechglow)
            TweenMax.to(bootdoormechglow.rotation, 2, { z: 0, ease: Power1.easeIn });
        if (orvmLight1 != null) {// console.log(">>>>>>>>")
            TweenMax.to(orvmLight1.rotation, 2, { y: 0, ease: Power1.easeIn });
        }
        if (orvmLight2 != null) {
            TweenMax.to(orvmLight2.rotation, 2, { y: 0, ease: Power1.easeIn });
        }
    }
    if (!doorOpen) doorOpen = true;
    else doorOpen = false;
}

var lightOn = false;
one3dFunctions.prototype.ToggleLights = function ToggleLights() {
    if (!lightOn) {
        if (
            ONE3D.variantName == "Turbo CVT XV Pre" ||
            ONE3D.variantName == "Turbo CVT XV" ||
            ONE3D.variantName == "Turbo MT XV Pre" ||
            ONE3D.variantName == "Turbo MT XV" ||
            ONE3D.variantName == "MT XV Pre" ||
            ONE3D.variantName == "MT XV"
        )
            one3dref.getMesh("NISSAN_FEAT_LED_FOG_LAMP_ON_MESH").setEnabled(true);

        one3dref.getMesh("NISSAN_FEAT_HEADLIGHT_LED_ON_MESH").setEnabled(true);
        one3dref.getMesh("NISSAN_FEAT_TAIL_GLOW_BODY_MESH").setEnabled(true);
        one3dref.getMesh("NISSAN_FEAT_TAIL_GLOW_BOOTDOOR_MESH").setEnabled(true);
        one3dref.getMesh("MAGNITE_ENV_NIGHT_LIGHTS").setEnabled(true);
        one3dref.getMaterial("MAGNITE_ENV_NIGHT_LIGHTS_MAT").alphaCutOff = 0;
        lightOn = true;
        if (lightInterval) {
            window.clearInterval(lightInterval);
            lightInterval = null;
        }
        for (var i = 0; i < timeoutsLight.length; i++) {
            clearTimeout(timeoutsLight[i]);
        }
        timeoutsLight = [];
        if (ONE3D.variantName == "Turbo CVT XV Pre") {
            var obj1 = one3dref.getMesh("MAGNITE_TURBO_CVT_XV_PRE_EX_INDICATOR_GLOW_MESH");
            var obj2 = one3dref.getMesh("MAGNITE_TURBO_CVT_XV_PRE_EX_L_DOOR_INDICATOR_MESH");
            var obj3 = one3dref.getMesh("MAGNITE_TURBO_CVT_XV_PRE_EX_R_DOOR_INDICATOR_MESH");
            obj1.setEnabled(true);
            obj2.setEnabled(true);
            obj3.setEnabled(true);
            var x = 0;
            lightInterval = setInterval(function () {
                obj1.setEnabled(false);
                obj2.setEnabled(false);
                obj3.setEnabled(false);

                jittercam();
                timeoutsLight.push(setTimeout(() => {
                    obj1.setEnabled(true);
                    obj2.setEnabled(true);
                    obj3.setEnabled(true);
                    jittercam();
                }, 500));
                if (++x === 5) {
                    if (lightInterval) {
                        //console.log(">>>>>>>>>");
                        window.clearInterval(lightInterval);
                        lightInterval = null;
                    }
                    for (var i = 0; i < timeoutsLight.length; i++) {
                        clearTimeout(timeoutsLight[i]);
                    }
                    timeoutsLight = [];
                }
            }, 1000);
        }
        lightOn = true;
    }
    else {
        if (lightInterval) {
            window.clearInterval(lightInterval);
            lightInterval = null;
        }
        for (var i = 0; i < timeoutsLight.length; i++) {
            clearTimeout(timeoutsLight[i]);
        }
        timeoutsLight = [];
        one3dref.getMesh("NISSAN_FEAT_HEADLIGHT_LED_ON_MESH").setEnabled(false);
        one3dref.getMesh("NISSAN_FEAT_LED_FOG_LAMP_ON_MESH").setEnabled(false);
        one3dref.getMesh("NISSAN_FEAT_TAIL_GLOW_BODY_MESH").setEnabled(false);
        one3dref.getMesh("NISSAN_FEAT_TAIL_GLOW_BOOTDOOR_MESH").setEnabled(false);
        one3dref.getMesh("MAGNITE_ENV_NIGHT_LIGHTS").setEnabled(false);
        if (ONE3D.variantName == "Turbo CVT XV Pre") {
            one3dref.getMesh("MAGNITE_TURBO_CVT_XV_PRE_EX_INDICATOR_GLOW_MESH").setEnabled(false);
            one3dref.getMesh("MAGNITE_TURBO_CVT_XV_PRE_EX_L_DOOR_INDICATOR_MESH").setEnabled(false);
            one3dref.getMesh("MAGNITE_TURBO_CVT_XV_PRE_EX_R_DOOR_INDICATOR_MESH").setEnabled(false);
        }
        //  TweenMax.to(one3dref.getMaterial('AIRCROSS_LONE_EX_LIGHTS_TAILLIGHT_WHITE_LIGHT').emissiveColor, 0.25, { r: 0.01, g: 0.0, b: 0.0, ease: Power1.easeIn, delay: 0, onUpdate: jittercam });
        lightOn = false;
    }
    jittercam();
};
// mainjs to exterional function calling
var CallbackHandler = (function () {
    return {
        _onLoadCarCompleteExterior: function () {

            ONE3D.resizeWindow();

            if (typeof onLoadCarCompleteExterior === 'function')
                window.onLoadCarCompleteExterior();
        },

        _onLoadCarCompleteInterior: function () {

           
            if (window.orientation == 0) {//POTRAIT
                if (window.one3dref)
                    window.one3dref._setToPotrait();
            }
            else {//LANDSCAPE
                if (window.one3dref)
                    window.one3dref._setToLandscape();
            }

            if (untouched == true) {
                setTimeout(cameratutorial, 2000);
            }
            ONE3D.generateAccList();
           

            checkalphaindex();
            ONE3D.changeBgColor('#c6d3d9');
            one3dref.getMaterial('MAGNITE_TURBO_CVT_XV_PRE_EX_WINDSHEILD_MAT').needDepthPrePass = true;
            // one3dref.getMaterial('NISSAN_FEAT_AIRBAG_MAT').needDepthPrePass = true;

            ONE3D.resizeWindow();

            if (typeof onLoadCarCompleteInterior === 'function')
                window.onLoadCarCompleteInterior();

            // if (typeof dialog != 'undefined') {
            //     showui();
            // }

            ONE3D.featureHotspot(false);
        },
        _onInteriorViewComplete: function () {

            ONE3D.resizeWindow();
            if (typeof onInteriorViewComplete === 'function')
                window.onInteriorViewComplete();
        },
        _onExteriorViewComplete: function () {

            ONE3D.resizeWindow();

            if (typeof onExteriorViewComplete === 'function')
                window.onExteriorViewComplete();
        },
        _onBackseatViewComplete: function () {
            ONE3D.resizeWindow();
            if (typeof onBackSeatViewComplete === "function")
                window.onBackSeatViewComplete();
        },
        _onFrontseatViewComplete: function () {
            ONE3D.resizeWindow();
            if (typeof onFrontViewComplete === "function")
                window.onFrontViewComplete();
        },
        _onVariantComplete: function (variantName) {
            ONE3D.variantName = variantName;
            ONE3D.resizeWindow();
            setdefaultcolor(variantName);
            ONE3D.generateAccList();


            if (typeof onVariantComplete === 'function')
                window.onVariantComplete();
                ONE3D.featureHotspot(false);

        },
        _checkOrientation: function () {
            if (window.innerHeight > window.innerWidth) {//POTRAIT
                window.one3dref._setToPotrait();
            }
            else {//LANDSCAPE
                window.one3dref._setToLandscape();
            }
        },
        _onFeatureHotspot: function (featureName) {
            if (untouched == true) {
                removecameratutorial();
            }

            if (doorOpen == true) {
                ONE3D.ToggleDoors();
            }

            if (typeof OnfeatureHpSelect === 'function') {
                window.OnfeatureHpSelect(featureName);
            }

        },
        _OnAnimationStart: function (featureName) {
            CurrentFeatName = featureName; console.log(featureName);
            hideflag = true;
            if (typeof OnAnimationStart === 'function')
                window.OnAnimationStart(featureName);
        },
        _OnAnimationComplete: function () {

            if (typeof OnAnimationComplete === 'function')
                window.OnAnimationComplete();
        },
        _checkOrientation: function () {
            if (window.innerHeight > window.innerWidth) {//POTRAIT
                window.angularComponentRef._setToPotrait();
            }
            else {//LANDSCAPE
                window.angularComponentRef._setToLandscape();
            }
        },
        _onLoaderProgress: function (value) {

            // document.getElementById("myBar").textContent = value + '%';
            document.getElementById("myBar").style.display = 'block';


            if (ONE3D.hasOwnProperty("loadedPercent")) {
                ONE3D.loadedPercent = value;
            }

            if (typeof onLoaderProgress === 'function')
                window.onLoaderProgress();
        },
        _onAccessoriesHotspot: function (data) {
            one3dref._setAccCam(data.hotspot);

            if (typeof onAccessoriesHotspot === 'function')
                window.onAccessoriesHotspot(featureName);
        },
        _onAccLoadingComplete: function () {
            //AccLoading = false;
            //Reset Accessorie camera
            // setTimeout(() => {
            //     ONE3D.resetAccCamera();
            // }, 2000);		

            if (typeof onAccLoadingComplete === 'function')
                window.onAccLoadingComplete();
        },

    }
})(CallbackHandler || {});
one3dFunctions.prototype.ChangeColor = function (hexcode, roofHexcode = null, customproperties = null) {
    if (hexcode == "flaregarnetred_onyxblack") {
        hexcode = "#9C0F14,#850D11";
        roofHexcode = "#050505,#1C1C1C";
        customproperties = {
            MAGNITE_TURBO_CVT_XV_PRE_EX_CARPAINT_MAT: { metallic: 0.85 },
        };
        customproperties = {
            MAGNITE_TURBO_CVT_XV_PRE_EX_CARPAINT_TWO_MAT: { metallic: 1.0 },
        };
    } else if (hexcode == "flaregarnetred") {
        hexcode = "#9C0F14,#850D11";
        roofHexcode = "#9C0F14,#850D11";
        customproperties = {
            MAGNITE_TURBO_CVT_XV_PRE_EX_CARPAINT_MAT: { metallic: 0.85 },
        };
        customproperties = {
            MAGNITE_TURBO_CVT_XV_PRE_EX_CARPAINT_TWO_MAT: { metallic: 0.85 },
        };
    } else if (hexcode == "pearlwhite_onyxblack") {
        hexcode = "#DFE0D9,#575757";
        roofHexcode = "#050505,#1C1C1C";
        customproperties = {
            MAGNITE_TURBO_CVT_XV_PRE_EX_CARPAINT_MAT: { metallic: 0.85 },
        };
        customproperties = {
            MAGNITE_TURBO_CVT_XV_PRE_EX_CARPAINT_TWO_MAT: { metallic: 1.0 },
        };
    } else if (hexcode == "sandstonebrown") {
        hexcode = "#382C29,#423D3D";
        roofHexcode = "#382C29,#423D3D";
        customproperties = {
            MAGNITE_TURBO_CVT_XV_PRE_EX_CARPAINT_MAT: { metallic: 1.0 },
        };
        customproperties = {
            MAGNITE_TURBO_CVT_XV_PRE_EX_CARPAINT_TWO_MAT: { metallic: 1.0 },
        };
    } else if (hexcode == "vivdblue_stormwhite") {
        hexcode = "#1367B0,#1367B0";
        roofHexcode = "#F2F2F2,#858484";
        customproperties = {
            MAGNITE_TURBO_CVT_XV_PRE_EX_CARPAINT_MAT: { metallic: 0.85 },
        };
        customproperties = {
            MAGNITE_TURBO_CVT_XV_PRE_EX_CARPAINT_TWO_MAT: { metallic: 0.5 },
        };
    } else if (hexcode == "bladesilver") {
        hexcode = "#787878,#707070";
        roofHexcode = "#787878,#707070";
        customproperties = {
            MAGNITE_TURBO_CVT_XV_PRE_EX_CARPAINT_MAT: { metallic: 1.0 },
        };
        customproperties = {
            MAGNITE_TURBO_CVT_XV_PRE_EX_CARPAINT_TWO_MAT: { metallic: 1.0 },
        };
    } else if (hexcode == "onyxblack") {
        hexcode = "#050505,#1C1C1C";
        roofHexcode = "#050505,#1C1C1C";
        customproperties = {
            MAGNITE_TURBO_CVT_XV_PRE_EX_CARPAINT_MAT: { metallic: 1.0 },
        };
        customproperties = {
            MAGNITE_TURBO_CVT_XV_PRE_EX_CARPAINT_TWO_MAT: { metallic: 1.0 },
        };
    } else if (hexcode == "stormwhite") {
        hexcode = "#F2F2F2,#858484";
        roofHexcode = "#F2F2F2,#858484";
        customproperties = {
            MAGNITE_TURBO_CVT_XV_PRE_EX_CARPAINT_MAT: { metallic: 0.4 },
        };
        customproperties = {
            MAGNITE_TURBO_CVT_XV_PRE_EX_CARPAINT_TWO_MAT: { metallic: 0.4 },
        };
    } else if (hexcode == "tourmalinebrownonyxblack") {
        hexcode = "#291717,#5C3938";
        roofHexcode = "#050505,#1C1C1C";
        customproperties = {
            MAGNITE_TURBO_CVT_XV_PRE_EX_CARPAINT_MAT: { metallic: 1.0 },
        };
        customproperties = {
            MAGNITE_TURBO_CVT_XV_PRE_EX_CARPAINT_TWO_MAT: { metallic: 1.0 },
        };
    } else {
        return 0;
    }
    ONE3D.resizeWindow();
    window.one3dref._changeColor(hexcode, roofHexcode, customproperties);
}


var hotsptData = {
    'FEAT_LED_PROJECTOR_HEADLAMP': { 'img': 'one3d/features/bi-projector_led_hadlamps_with_led_turn_indicators.jpg', 'title': 'FIRST IN SEGMENT BI-PROJECTOR LED HEADLAMPS WITH LED TURN INDICATORS', 'details': "With the 1st-in-segment sleek Bi-Projector LED Headlamps With LED Turn indicators, the road ahead shall always look clear as day, any time and every time." },
    'FEAT_DIAMONDCUT_ALLOY': { 'img': 'one3d/features/diamond_cut_16_inch_alloy_wheel.jpg', 'title': 'DIAMOND CUT 16 INCH ALLOY HOUSED IN SQUARE WHEEL ARCHES and high ground clearance of 205MM', 'details': '' },
    'FEAT_LED_DRL': { 'img': 'one3d/features/l-shapred_led_drls_with_led_fog_lamps.jpg', 'title': 'L-SHAPED LED DRLS WITH LED FOG LAMPS', 'details': "Outshine the spotlight wherever you go. The all new Nissan Magnite’s L-Shaped Daytime Running Lamps (DRLs) and LED fog lamps help you make style statements effortlessly." },
    'FEAT_TAIL_LAMP': { 'img': 'one3d/features/wide_split_signature_tail_lamp.jpg', 'title': 'WIDE SPLIT SIGNATURE TAIL LAMP', 'details': "While you’re zipping through the streets, leave a lasting impression with the sporty and sharp Wide Split Signature Taillamps." },
    'FEAT_ROOF_RAILS': { 'img': 'one3d/features/roof_rails.jpg', 'title': 'SPORTY ROOF RAILS', 'details': "Functional and stylish roof rails that can bear a load of upto 50 Kgs." },
    'FEAT_KNEE_ROOM': { 'img': 'one3d/features/best_in_class_rear_kneeroom.jpg', 'title': 'BEST IN CLASS REAR KNEEROOM', 'details': "Get no complaints about space from your family or friends sitting in the backseat. Find maximum space for their knees, shoulders and minds." },
    'FEAT_DRIVE_ASSIST': { 'img': 'one3d/features/advanced_drive_Assist_display_with_welcome_animation.jpg', 'title': 'BEST IN SEGMENT ADVANCED DRIVE ASSIST DISPLAY WITH WELCOME ANIMATION', 'details': 'Feel more confident and focused with all your driving information, safety features & much more on the 7" TFT display.' },
    'FEAT_COCKPIT_INTERFACE': { 'img': 'one3d/features/cockpit_interface_design.jpg', 'title': 'BEST-IN-CLASS COCKPIT INTERFACE DESIGN', 'details': "Keeping you in the centre, we have built the best-in-class cockpit interface design in the all new Nissan Magnite. It comes with a 5-degree tilt of the centre stack which ensures you don’t just drive out, you fly out." },
    'FEAT_CARGO_SPACE': { 'img': 'one3d/features/336L_cargo_space_with_6040_split.jpg', 'title': '336L CARGO SPACE WITH 60:40 SPLIT', 'details': "Invite Possibilities, not excuses. With a 336L cargo space, this car makes space for your memories and lifestyle, no matter where you go." },
    'FEAT_HRAO_CVT': { 'img': 'one3d/features/advanced_HRA0_X-tronic-cvt.jpg', 'title': 'FIRST-IN-SEGMENT MOST ADVANCED HRA0 X-TRONIC CVT', 'details': "Be it bumper to bumper traffic or highways that vanish at the horizon, all new Nissan Magnite’s CVT provides refined power delivery for effortless & smooth driving experiences." },
    'FEAT_WIRELESS_CONNECT': { 'img': 'one3d/features/wireless_connectivity_to_android_auto_and_apple_careplay.jpg', 'title': 'BEST-IN-CLASS WIRELESS CONNECTIVITY TO ANDROID AUTO & APPLE CARPLAY', 'details': "The classy 8” full flush touchscreen provides non-stop entertainment with wireless connectivity to Android Auto & Apple CarPlay." },
    'FEAT_SAFETY': { 'img': 'one3d/features/ABS_EBD_airbag.jpg', 'title': 'ANTI-LOCK BRAKING SYSTEM (ABS) WITH ELECTRONIC BRAKEFORCE DISTRIBUTION (EBD) AND DUAL AIRBAGS', 'details': "A road full of surprises can never be a threat with two intuitive safety systems in the all new Nissan Magnite – ABS & EBD. To make it a safe cocoon it also comes with dual airbags." },
    'FEAT_TECH': { 'img': 'one3d/features/tech_up_your_drive.jpg', 'title': 'TECH UP YOUR DRIVE', 'details': "While the all new Nissan Magnite is sure to make a style statement, make yours stand out. Check out our combinations of tech and innovations for your convenience and comfort." },
    'FEAT_360_CAMERA': { 'img': 'one3d/features/around_view_camera.jpg', 'title': 'FIRST IN CLASS AROUND VIEW CAMERA', 'details': "See more, do more with 360 degree around view camera." },
    'FEAT_HRAO_TURBO': { 'img': 'one3d/features/HRA0_1.0L_turbo_engine.jpg', 'title': 'HRAO 1.0L TURBO ENGINE', 'details': "Now, start every journey with the power of HRAO 1.0L engine coursing through the car, resulting in quicker and smoother acceleration." },
    'FEAT_COCKPIT_INTERFACE_XL': { 'img': 'one3d/features/CVT_XL_Cockpit.jpg', 'title': 'BEST-IN-CLASS COCKPIT INTERFACE DESIGN', 'details': "Keeping you in the centre, we have built the best-in-class cockpit interface design in the all new Nissan Magnite. It comes with a 5-degree tilt of the centre stack which ensures you don’t just drive out, you fly out." },
    'FEAT_COCKPIT_INTERFACE_XE': { 'img': 'one3d/features/MT_XE_Cockpit.jpg', 'title': 'BEST-IN-CLASS COCKPIT INTERFACE DESIGN', 'details': "Keeping you in the centre, we have built the best-in-class cockpit interface design in the all new Nissan Magnite. It comes with a 5-degree tilt of the centre stack which ensures you don’t just drive out, you fly out." }
};

function OnfeatureHpSelect(hpName) {
    if (hpName.hotspot == 'FEAT_Headlamp_LED_Projector') {
         data = showHotspot('FEAT_LED_PROJECTOR_HEADLAMP', '2d');
    }
    if (hpName.hotspot == 'FEAT_LED_PROJECTOR_HEADLAMP') {
        return showHotspot('FEAT_LED_PROJECTOR_HEADLAMP', '2d');
    }
    if (hpName.hotspot == 'FEAT_DIAMONDCUT_ALLOY') {
        return showHotspot('FEAT_DIAMONDCUT_ALLOY', '2d');
    }
    if (hpName.hotspot == 'FEAT_LED_DRL') {
        return showHotspot('FEAT_LED_DRL', '2d');
    }
    if (hpName.hotspot == 'FEAT_TAIL_LAMP') {
        return showHotspot('FEAT_TAIL_LAMP', '2d');
    }
    if (hpName.hotspot == 'FEAT_ROOF_RAILS') {
        return showHotspot('FEAT_ROOF_RAILS', '2d');
    }
    if (hpName.hotspot == 'FEAT_KNEE_ROOM') {
        return showHotspot('FEAT_KNEE_ROOM', '2d');
    }
    if (hpName.hotspot == 'FEAT_DRIVE_ASSIST') {
        return showHotspot('FEAT_DRIVE_ASSIST', '2d');
    }
    if (hpName.hotspot == 'FEAT_COCKPIT_INTERFACE') {
        return showHotspot('FEAT_COCKPIT_INTERFACE', '2d');
    }
    if(hpName.hotspot == 'FEAT_COCKPIT_INTERFACE_XL')
    {
        return showHotspot('FEAT_COCKPIT_INTERFACE_XL','2d');
    }
    if(hpName.hotspot == 'FEAT_COCKPIT_INTERFACE_XE')
    {
        return showHotspot('FEAT_COCKPIT_INTERFACE_XE','2d');
    }
    if(hpName.hotspot == 'FEAT_CARGO_SPACE')
    {
        return showHotspot('FEAT_CARGO_SPACE','2d');
    }
    if (hpName.hotspot == 'FEAT_HRAO_CVT') {
        return showHotspot('FEAT_HRAO_CVT', '2d');
    }
    if (hpName.hotspot == 'FEAT_WIRELESS_CONNECT') {
        return showHotspot('FEAT_WIRELESS_CONNECT', '2d');
    }
    if (hpName.hotspot == 'FEAT_SAFETY') {
        return showHotspot('FEAT_SAFETY', '2d');
    }
    if (hpName.hotspot == 'FEAT_TECH') {
        return showHotspot('FEAT_TECH', '2d');
    }
    if (hpName.hotspot == 'FEAT_360_CAMERA') {
        return showHotspot('FEAT_360_CAMERA', '2d');
    }
    if (hpName.hotspot == 'FEAT_HRAO_TURBO') {
        return showHotspot('FEAT_HRAO_TURBO', '2d');
    }
    
}
var animation = false;
function showHotspot(name, featuretype) {
    data={"imagename":hotsptData[name]['img'],
            "imagetitle":hotsptData[name]['title'],
            "imagedetails":hotsptData[name]['details'],
            "featuretype":featuretype
                }
    if (featuretype == '2d') {
        animation = false;
        return  data
    }
    else {
        animation = true;
        ONE3D.showFeature(name);
        return  null
    }
}

//Accessories
//Set Accessories Camera
one3dFunctions.prototype.setAccCamera = function (value) {
    if (window.one3dref)
        window.one3dref._setAccCam(value);
}

// Reset from Accessories camera
one3dFunctions.prototype.resetAccCamera = function () {
    console.log("resetAcc Camera")

    if (window.one3dref)
        window.one3dref._ResetAccCamera();

    // if(_accTween != undefined)
    // {
    //     _accTween.kill();
    //     _accTween = undefined;
    //     DisableHighlight();
    // }
}

// Remove Accessories
one3dFunctions.prototype.removeAccessories = function (AccID) {

    AccID = AccID;
    var tagName = null
    if (AccList[AccID] != undefined && AccList[AccID].hasOwnProperty("tag") && AccList[AccID].tag != undefined)
        tagName = AccList[AccID].tag;

    console.log(AccID)
    console.log(tagName)
    if (window.one3dref)
        window.one3dref._RemoveAccessories(AccID, null);

    // DisableHighlight();
}

var AccLoading = false;
one3dFunctions.prototype.addAccessories = function (AccID) {
    
    if (lightOn) ONE3D.ToggleLights();
    if (doorOpen) ONE3D.ToggleDoors();
    if(untouched)
    removecameratutorial();
 
    console.log("AccID");
    console.log(AccID);

    if (AccList[AccID] == undefined) {
        AccLoading = false;
        console.error("Accessories ID not Found in Data !")
        return;
    }
    else {
        var tagName = AccList[AccID].tag;
        if (HpList[tagName] == undefined) {
            AccLoading = false;
            console.error("Camera Data Not Found");
            return;
        }
        else {
            var hpName = HpList[tagName];
            ONE3D.setAccCamera(hpName);
            // acctween(tagName);
            setTimeout(() => {
                window.one3dref._AddAccessories(AccID, tagName);
            }, 300);
        }
    }
}

one3dFunctions.prototype.removeAllAccessories = function () {
    one3dref._removeAllAcc();
}

var merge, AccList = {}, HpList = {};
one3dFunctions.prototype.generateAccList = function () {

    /// Accessories List
    AccList = {};
    HpList = {};
    var mat = one3dref.getMatJson();
    var Ext = mat[ONE3D.carName][ONE3D.variantName].Accessories.Accessory_Exterior;
    var Int = mat[ONE3D.carName][ONE3D.variantName].Accessories.Accessory_Interior;
    merge = Object.assign(Ext, Int);
    console.log(merge);

    Object.keys(merge).forEach(element => {
        merge[element].forEach(element => {
            AccList = Object.assign(AccList, element);
        });
    });

    // Hp List
    var Anim = one3dref.getAnimJson();
    Object.keys(Anim[ONE3D.variantName]).forEach(element => {
        //console.log(Anim[ONE3D.variantName][element]);
        if (Anim[ONE3D.variantName][element].hasOwnProperty("animation") && Anim[ONE3D.variantName][element].animation[0].hasOwnProperty("tag")) {
            var hp_tagName = Anim[ONE3D.variantName][element].animation[0].tag;
            HpList[hp_tagName] = element;

        }
    });
    //console.log(HpList);
}
one3dFunctions.prototype.featureHotspot = function (value) {
    window.one3dref._showHotSpot(value);
    HpVisible = value;
}

function setdefaultcolor(variantname) {
    if (variantname == "MT XE") {
      updatecolor("color-blade-silver", "Blade Silver", "bladesilver");
    } else if (
      variantname == "MT XL" ||
      variantname == "Turbo MT XL" ||
      variantname == "Turbo CVT XL"
    ) {
      updatecolor("color-blade-silver", "Blade Silver", "bladesilver");
    } else if (
      variantname == "MT XV" ||
      variantname == "Turbo MT XV" ||
      variantname == "Turbo CVT XV"
    ) {
      updatecolor(
        "color-white-n-blue",
        "Vivid Blue & Storm White",
        "vivdblue_stormwhite"
      );
    } else if (
      variantname == "MT XV Pre" ||
      variantname == "Turbo MT XV Pre" ||
      variantname == "Turbo CVT XV Pre"
    ) {
      updatecolor(
        "color-black-n-red",
        "Flare Garnet Red & Onyx Black",
        "flaregarnetred_onyxblack"
      );
    }
  }

  function updatecolor(code, value, color) {
    ONE3D.ChangeColor(color);
  }